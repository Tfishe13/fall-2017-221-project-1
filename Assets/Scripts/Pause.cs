﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{

    //public static Pause instance = null;
    //public UnityEngine.UI.Image image;
    bool isPaused = false;
    //public Rect windowRect = new Rect(20, 20, 120, 50);
    public GameObject paused;
    public GameObject player1;
    public GameObject player2;
    public GameObject ball;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            PauseButton();
        }
    }
    void PauseButton()
    {
        if (isPaused == false)
        {
            Time.timeScale = 0f;
            paused.SetActive(true);
            isPaused = true;
            player1.GetComponent<MovementLeft>().isPasued = true;
            player2.GetComponent<MovementRight>().isPaused = true;
            ball.GetComponent<PongBallMove>().isPaused = true;
        }
        else
        {
            Time.timeScale = 1.0f;
            paused.SetActive(false);
            isPaused = false;
            player1.GetComponent<MovementLeft>().isPasued = false;
            player2.GetComponent<MovementRight>().isPaused = false;
            ball.GetComponent<PongBallMove>().isPaused = false;
        }
    }
}  



        //if (image.gameObject.activeInHierarchy == false)
        //{
        //  image.gameObject.SetActive(true);
        //  Time.timeScale = 0;
        //  isPaused = true;
        //}
        //else if (image.gameObject.activeInHierarchy == true)
        //{
        //  image.gameObject.SetActive(false);
        //  Time.timeScale = 1;
        //  isPaused = false;
        //}
    //}
  //public void OnGUI()
  //{
  //  GUI.enabled = false;
  //  if (isPaused == true)
  //  {
  //    GUI.enabled = true;
  //    //GUI.Window(0, windowRect, DoMyWindow, "Paused");
  //  }

  //}
  //void DoMyWindow(int windowID)
  //{
  //  if (GUI.Button(new Rect(10, 20, 100, 20), "Hello World"))
  //    print("Paused");
  //}
