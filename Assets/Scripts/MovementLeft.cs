﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementLeft : MonoBehaviour
{
    public GameObject leftPlayer;
    public float moveSpeed;
    public bool isPasued = false;

    public Audio_Manager audioManager;
    public AudioClip pongLeftSound;

    private int minY = 2, maxY = 12;

    /*void Start()
    {
        audioManager = GameObject.FindWithTag("AudioManager").GetComponent<Audio_Manager>();
    }*/

    void Update ()
    {
        if(isPasued == false)
        {
            float verticalInput = Input.GetAxis("P1_VerticalAxis") * moveSpeed * Time.deltaTime;

            transform.Translate(0f, verticalInput, 0f);

            Vector3 tempVector = transform.position;

            tempVector.y = Mathf.Clamp(tempVector.y, minY, maxY);

            transform.position = tempVector;
        }
     }

    /*void OnCollisionEnter(Collision col)
    {
        audioManager.PlaySFX(pongLeftSound);
    }*/
}
